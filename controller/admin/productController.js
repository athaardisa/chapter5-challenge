const imagekit = require("../../lib/imagekit");
const { products } = require("../../models");
const { Op, and } = require("sequelize");

// get products
async function getProducts(req, res) {
  const responseData = await products.findAll();
  res.render("index", {
    data: responseData,
    alert: req.flash("alert"),
    alertErr: req.flash("alertErr"),
    alertUp: req.flash("alertUp"),
    alertDel: req.flash("alertDel"),
  });
}

// create product
async function createProduct(req, res) {
  try {
    // proses file naming
    const split = req.file.originalname.split(".");
    console.log(split);
    const extension = split[split.length - 1];
    const imageName = req.file.originalname + "." + extension;

    // upload file
    const img = await imagekit.upload({
      file: req.file.buffer,
      fileName: imageName,
    });
    const { name, size, price, userId } = req.body;
    await products.create({
      name: name,
      size: size,
      price: price,
      imgUrl: img.url,
      userId: userId,
    });
    req.flash("alert", "Data Berhasil Disimpan");
    res.redirect("/");
  } catch (error) {
    req.flash("alertErr", "Data Gagal Disimpan");
    res.redirect("/");
  }
}

// update product
async function updateProduct(req, res) {
  try {
    if (req.file != undefined) {
      const split = req.file.originalname.split(".");
      // console.log(split);
      const extension = split[split.length - 1];
      const imageName = req.file.originalname + "." + extension;

      // upload file
      const img = await imagekit.upload({
        file: req.file.buffer,
        fileName: imageName,
      });
      const { name, size, price } = req.body;
      const imgUrl = img.url;
      const id = req.params.id;
      await products.update(
        { name, size, price, imgUrl },
        {
          where: { id },
        }
      );
      req.flash("alertUp", "Data Berhasil Diupdate");
      res.redirect("/");
    } else {
      const { name, size, price } = req.body;
      const id = req.params.id;
      await products.update(
        { name, size, price },
        {
          where: { id },
        }
      );
      req.flash("alert", "Data Berhasil Diupdate");
      res.redirect("/");
    }
  } catch (error) {
    req.flash("alertErr", "Data Gagal Diupdate");
    res.redirect("/");
  }
}

// delete product
async function deleteProduct(req, res) {
  try {
    const id = req.params.id;
    await products.destroy({ where: { id } });
    req.flash("alertDel", "Data Berhasil Dihapus");
    res.redirect("/");
  } catch (error) {
    req.flash("alertErr", "Data Gagal Dihapus");
    res.redirect("/");
  }
}

// category Product
async function categoryProducts(req, res) {
  let data;
  data = await products.findAll({
    where: {
      [Op.and]: [
        // { name: req.query.name },
        { size: req.query.size },
      ],
    },
  });
  res.render("index", {
    data,
    alert: req.flash("alert"),
    alertErr: req.flash("alertErr"),
    alertUp: req.flash("alertUp"),
    alertDel: req.flash("alertDel"),
  });
}

// search Product
async function searchProducts(req, res) {
  let data;
  data = await products.findAll({
    where: {
      name: {
        [Op.or]: {
          [Op.iLike]: req.query.name,
          [Op.substring]: req.query.name,
        },
      },
    },
  });
  res.render("index", {
    data,
    alert: req.flash("alert"),
    alertErr: req.flash("alertErr"),
    alertUp: req.flash("alertUp"),
    alertDel: req.flash("alertDel"),
  });
}

module.exports = {
  getProducts,
  createProduct,
  deleteProduct,
  updateProduct,
  categoryProducts,
  searchProducts,
};

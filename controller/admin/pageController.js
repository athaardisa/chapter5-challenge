const { products } = require("../../models");

// create Product page
async function createProductPage(req, res) {
  res.render("add");
}
// update page
async function updateProductPage(req, res) {
  const id = req.params.id;
  const productData = await products.findByPk(id);
  res.render("edit", {
    data: productData,
  });
}

// get id to modal
async function deleteProductModal(req, res) {
  const id = req.params.id;
  const productData = await products.findByPk(id);
  res.render("edit", {
    data: productData,
  });
}

module.exports = {
    createProductPage,
    updateProductPage
}

const imagekit = require('../lib/imagekit')
const {products} = require('../models')

// get all data
async function getProducts (req, res){
        try {
            const responseData = await products.findAll()
            res.status(200).json({
                'data': responseData
            })
        } catch (err) {
            console.log(err.message)
        }
}

// get by id
async function getProductById(req, res) {
    try {
        const id = req.params.id
        const product = await products.findByPk(id)
        // console.log(product)
        if (product === null) {
            res.status(404).json({
                'message': `data pada ${id} tersebut tidak ada`
            })
        }

        res.status(200).json({
            'data': product
        })
    } catch (err) {
        console.log(err.message)
    }
}

// create data
async function createProduct (req, res){
    try {
        // process file naming        
        const split = req.file.originalname.split('.')
        // console.log(split)
        const extension = split[split.length - 1]
        // console.log(extension)

        const imageName = req.file.originalname + '.' + extension
        // console.log(imageName)

        // upload file 
        const img = await imagekit.upload({
            file: req.file.buffer,
            fileName: imageName
        })

        // console.log(img)

        const {name, size, price, userId} = req.body
        const responseData = await products.create({
            name: name,
            size: size,
            price: price,
            imgUrl: img.url,
            userId: userId
        })
        res.status(200).json({
            'data': product
        })
    } catch (err) {
        console.log(err.message)
    }
}

// update data
async function updateProduct(req, res) {
    try {
        const split = req.file.originalname.split('.')
        // console.log(split)
        const extension = split[split.length - 1]
        // console.log(extension)

        const imageName = req.file.originalname + '.' + extension
        // console.log(imageName)

        // upload file 
        const img = await imagekit.upload({
            file: req.file.buffer,
            fileName: imageName
        })
        const {name, size, price, userId} = req.body
        const imgUrl = img.url
        const id = req.params.id
        const responseData = await products.update({name,size,price,imgUrl,userId}, {
            where: {id}
        })
        res.status(200).json({
            'success': true
        })
    } catch (err) {
        console.log(err.message)
    }
}

// delete data
async function deleteProduct (req, res){
    try {
        const id = req.params.id
        const responseData = await products.destroy({where: {id}})
        res.status(200).json({
            'success': true
        })
    } catch (err) {
        console.log(err.message)
    }
}

module.exports = {
    getProducts,
    getProductById,
    createProduct,
    updateProduct,
    deleteProduct,
    
}
const dotenv = require('dotenv')
dotenv.config()

const express = require("express")
const bodyParser = require("body-parser")
const path = require("path")
const morgan = require('morgan');
const cookieParser = require('cookie-parser')
const session = require('express-session')
const flash = require('connect-flash')

// our own module
const routes = require("./routes")

const port = 8000

// Intializations
const app = express();

// message alert
app.use(cookieParser("secret"))
app.use(
    session({
        cookie: {maxAge: 6000},
        secret: "secret",
        resave: true,
        saveUninitialized: true
    })
)
app.use(flash())
// Settings view engine
// app.set("views", __dirname + "/views");
app.set("view engine", "ejs");

// Set JSON
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
// app.use(express.json())

// Public
app.use(express.static("public"));
app.use(express.static("controller"));

app.use(morgan('dev'))

// Routes
app.use(routes)

app.get("/add", (req, res) => {
    res.render ("add")
})

// server
app.listen(port, () => {
    console.log(`server running on http://localhost:${port}`)
})
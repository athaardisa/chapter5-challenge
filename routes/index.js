const router = require('express').Router()

const productController = require('../controller/productController')
const adminProductController = require('../controller/admin/productController')
const pageController = require('../controller/admin/pageController')

const uploader = require('../middleware/uploader')

// API
router.get('/api/products', productController.getProducts)
router.get('/api/products/:id', productController.getProductById)
router.post('/api/products', uploader.single('imgUrl'), productController.createProduct)
router.put('/api/products/:id',uploader.single('imgUrl'), productController.updateProduct)
router.delete('/api/products/:id', productController.deleteProduct)

// admin
router.get('/', adminProductController.getProducts)
router.post('/admin/add', uploader.single('imgUrl'), adminProductController.createProduct)
router.get('/admin/delete/:id', adminProductController.deleteProduct)
router.post('/admin/edit/:id', uploader.single('imgUrl'), adminProductController.updateProduct)
router.get('/category', adminProductController.categoryProducts)
router.get('/search', adminProductController.searchProducts)

// page
router.get('/admin/edit/:id', pageController.updateProductPage)
router.get('/admin/add', pageController.createProductPage)

module.exports = router